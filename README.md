# README #

### What is this repository for? ###

Contains the AUSGeoid2020 grid files which model the offset between the ellipsoid and Australian Height Datum and the associated uncertainty.

### Who do I talk to? ###

For enquiries on the grid files, please contact Nicholas Brown from Geoscience Australia (nicholas.brown@ga.gov.au)